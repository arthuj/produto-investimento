package br.com.itau;

import java.security.PublicKey;
import java.util.Scanner;

public class Produto {
    private double valor;
    private int meses;

    public Produto(double valor, int meses) {
        this.valor = valor;
        this.meses = meses;
    }

    public Produto() {
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public int getMeses() {
        return meses;
    }

    public void setMeses(int meses) {
        this.meses = meses;
    }

    public Produto ReceberValor(Produto produto){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Por favor, digite o valor que será investido");
        produto.setValor(scanner.nextDouble());
        return produto;
    }

    public Produto ReceberMeses(Produto produto){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Por favor, digite o número de meses");
        produto.setMeses(scanner.nextInt());
        return produto;
    }

    public  void retornarValorInvestimento(String nome, double valor, int meses){
        for(int i = 1; i <= meses; i++)
        {
            valor = valor * 1.07;
        }
        System.out.println(nome + ", seu valor total investido será de " + valor);
    }
}
